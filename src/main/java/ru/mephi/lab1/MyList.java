package ru.mephi.lab1;

public class MyList {
    private int size;
    private int capacity;
    private int adds= 0; //coefficient for adding memory operations
    private Object[] items;

    public MyList(){
        this.items = new Object[16];
        this.size = 0;
        this.capacity = 16;
    }
    public MyList(Object[] array){
        if (array.length == 0)
            new MyList();
        else {
            this.items = new Object[array.length];
            System.arraycopy(array, 0, this.items, 0, array.length);
            this.size = array.length;
            this.capacity = this.size;
        }
    }
    private boolean isItemsEq(MyList list){
        boolean res = true;
        for (int i = 0; i<size; ++i)
            if (!(this.items[i].
                    equals(list.items[i]))) {
                res = false;
                break;
            }
        return res;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if  (!(obj instanceof MyList))
            return false;
        MyList list = (MyList) obj;

        return (
                this.size == list.size() &&
                        isItemsEq(list)
        );

    }

    @Override
    public String toString() {
        if (this.size == 0)
            return "Empty list";
        else {
            StringBuilder res = new StringBuilder("List: ");
            for (int i = 0; i < size; ++i)
                if (items[i] == null)
                    res.append("null").append(", ");
                else
                    if (i == size-1)
                        res.append(items[i].toString());
                    else
                        res.append(items[i].toString()).append(", ");
            return res.toString();
        }
    }

    // faster then Math.pow()
    private int  pow2(int i) {
        int res = 1;
        for (int k = 0; k < i; ++k){
            res = res*2;
        }
        return res;
    }

    /**
     * Add value at the beginning
     * @param value value to be inserted
     */
    public void add(Object value){
        if (size == 0) {
            this.items[0] = value;
            ++size;
            //++adds;
        }
        else {
            if (size == capacity) {
                int additionalSize = pow2(adds);
                Object[] newItems = new Object[this.capacity + additionalSize];
                System.arraycopy(this.items, 0, newItems, 0, this.size);
                items = newItems;

                items[this.size] = value;

                ++size;
                this.capacity += additionalSize;
                ++adds;

            }
            else {
                items[this.size] = value;
                ++size;
            }
        }
    }

    /**
     * Removes value at index
     * @param index index of value to remove
     * @return removed value
     */
    public Object remove(int index){
        if (index > (size - 1) || index < 0){
            return null;
        }
        Object removedObj = this.items[index];

        // reduce capacity if too much free space
        if (capacity - size + 1 > (3*pow2(adds -2))){
            int newCapacity = capacity - pow2(adds - 1);
            Object[] newItems = new Object[newCapacity];

            System.arraycopy(items,0,newItems,0,index);
            System.arraycopy(items,  index + 1, newItems,
                    index, size - index - 1 );

            items = newItems;
            --adds;
            capacity = newCapacity;
            --size;
        }
        else {
            System.arraycopy(items,index + 1, items,
                    index, size - index - 1);
            --size;
        }
        return removedObj;
    }

    /**
     * Adds value at index
     */
    public void add(Object value, int index){
        if (index <= (size - 1) && index > 0){
            /*
                If new allocation needed,
                add capacity based on number of previous adds and removes
            */
            if (size == capacity) {
                int additionalSize = pow2(adds);
                Object[] newItems = new Object[this.capacity + additionalSize];

                System.arraycopy(items, 0, newItems,
                        0, index);
                System.arraycopy(items,index, newItems,
                        +index + 1, size - index);

                newItems[index]  = value;

                items = newItems;
                ++size;
                this.capacity += additionalSize;
                ++adds;

            }
            else {
                System.arraycopy(items,index,items,index  + 1,index);
                items[index] = value;
                ++size;

            }
        }
    }

    /**
     * Returns value at index.
     * If index out of bounds, returns null
     * @param index
     * @return value at index or null
     */
    public Object get(int index){
        if (index > (size - 1) || index < 0)
            return null;
        return items[index];
    }

    /**
     * Returns first index of a value
     * @param value to be found
     * @return it's index in MyList
     */
    public int indexOf(Object value){
        int res = -1;
        for(int i = 0; i<size; ++i)
            if (value.equals(items[i])) {
                res = i;
                break;
            }
        return res;
    }

    /**
     * Check if MyList contains a value
     * @param value value to check
     * @return thue if contains
     */
    public boolean contains(Object value){
        boolean res = false;
        for(int i = 0; i<size; ++i)
            if (value.equals(items[i])) {
                res = true;
                break;
            }
        return res;
    }

    /**
     * Sets value at index
     * @param value value to insert
     * @param index index to replace
     * @return replaced object
     */
    public Object set(Object value, int index){
        if (index > (size - 1) || index < 0)
            return null;
        Object objToReplace = items[index];
        items[index] = value;
        return objToReplace;
    }
    public int size(){
        return size;
    }
    public boolean isEmpty(){
        return size == 0;
    }

}
