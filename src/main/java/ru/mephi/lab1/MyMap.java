package ru.mephi.lab1;
import java.util.ArrayDeque;



public class MyMap<K extends Comparable<K>> {

    private class Node{
        private K key;
        private Object value;
        private Node left;
        private Node right;


        Node(K key, Object value){
            this.key = key;
            this.value = value;
            this.left = null;
            this.right = null;
        }
    }
    private static class Pair<T1,T2>{
        T1 _1;
        T2 _2;
        @Override
        public String toString(){
            return "(" + _1 + "," + _2 + ")";
        }
        Pair(T1 _1, T2 _2){
            this._1 = _1;
            this._2 = _2;
        }
    }

    Node root;
    int size;

    /**
     * value returned from `get` method, if nothing matched
     */
    Object byDefault = null;

    private Node put(Node node, Node toInsert){
        if (node == null)
            return toInsert;
        else
            if (toInsert.key.compareTo(node.key) == 0){
                node.value = toInsert.value;
                return null;
            }
            if (toInsert.key.compareTo(node.key) > 0)
                node.right = put(node.right,toInsert);
            else
                node.left = put(node.left,toInsert);
            return node;
    }
    public MyMap(){
        this.size = 0;
        this.root = null;
    }

    public void put(K key, Object value){
        if(size == 0)
            this.root = new Node(key,value);
        else
            if (put(root,new Node(key,value)) != null)
                size++;
    }

    /**
     * @return Pair(parent of node,node)
     */
    private Pair<Node,Node> findNode(K key){
        ArrayDeque<Pair<Node,Node>> queue = new ArrayDeque<>();
        queue.add(new Pair<>(null,this.root));
        if (this.size != 0)
            while (!queue.isEmpty()) {
                Pair<Node,Node> currPair = queue.poll();
                Node currNode = currPair._2;

                if (currNode.key.compareTo(key) == 0)
                    return currPair;

                Node left = currNode.left;
                Node right = currNode.right;

                if (left != null)
                    queue.add(new Pair<>(currNode,left));

                if (right != null)
                    queue.add(new Pair<>(currNode,right));
            }
        return new Pair<>(null,null);
    }
    public Object get(K key)  {
        Pair<Node,Node> guess = findNode(key);
        if (guess._2 != null)
            return guess._2.value;
        else
            return byDefault;
    }

    public Object get(K key, Object byDefault){
        this.byDefault = byDefault;
        return get(key);
    }

    /**
     *  Finds rightmost Node in a tree, where `node` is a root
     *  @return  Pair(parent of node, node)
     */
    private Pair<Node,Node> findMax(Node node){
        Node currNode = node;
        Node parent = null;
        while (currNode.right != null){
            currNode = currNode.right;
            parent = currNode;
        }
        return new Pair<>(parent,currNode);
    }


    /**
     * Removes pair (key,value) by key
     * @param key
     * @return Removed value
     */
    public Object remove(K key){
        Pair<Node,Node> toRemove = findNode(key);
        Node node = toRemove._2;
        Node parent = toRemove._1;


        if (node == null)
            return null;

        Object toReturn = node.value;

        //node has both children
        if (node.right != null && node.left != null) {
            Pair<Node, Node> replacement = findMax(node.left);

            node.key = replacement._2.key;
            node.value = replacement._2.value;

            if (replacement._1 == null) //replacement is a left child
                node.left = replacement._2.left;
            else
                replacement._1.right = null;

            --this.size;
            return toReturn;
        }

        //node is root
        if (parent == null) {
            if (size == 1)
                this.root = null;
            if (node.left != null)
                this.root = node.left;
            if (node.right != null)
                this.root = node.right;
            --this.size;
            return toReturn;
        }
        //node has only right child
        if (node.right != null) {
            if (parent.left == node)
                parent.left = node.right;
            else
                parent.right = node.right;
            return toReturn;
        }
        //node has only left child
        if (node.left != null){
            if (parent.left == node)
                parent.left = node.left;
            else
                parent.right = node.left;
            return toReturn;
        }

        //node has no children
        if (parent.left == node)
            parent.left = null;
        else
            parent.right = null;

        --this.size;
        return toReturn;
    }

    /**
     * Checks if MyMap contains a key
     * @param key key to check
     * @return true if contains
     */
    public boolean keyContains(K key){
        ArrayDeque<Node> queue = new ArrayDeque<>();
        queue.add(this.root);
        if (this.size != 0)
            while (!queue.isEmpty()) {
                Node currNode = queue.poll();

                if (currNode.key.compareTo(key) == 0)
                    return true;

                Node left = currNode.left;
                Node right = currNode.right;

                if (left != null)
                    queue.add(left);

                if (right != null)
                    queue.add(right);
            }
        return false;
    }

    /**
     * Gets all of keys in MyMap
     * @return MyList with all keys
     */
    public MyList  getKeys(){
        ArrayDeque<Node> queue = new ArrayDeque<>();
        MyList keys = new MyList();
        queue.add(this.root);
        if (this.size != 0)
            while (!queue.isEmpty()) {
                Node currNode = queue.poll();

                keys.add(currNode.key);

                Node left = currNode.left;
                Node right = currNode.right;

                if (left != null)
                    queue.add(left);

                if (right != null)
                    queue.add(right);
            }
        return keys;
    }
    /**
     * Gets all of values in MyMap
     * @return MyList with all values
     */
    public MyList getValues(){
        ArrayDeque<Node> queue = new ArrayDeque<>();
        MyList values = new MyList();
        queue.add(this.root);
        if (this.size != 0)
            while (!queue.isEmpty()) {
                Node currNode = queue.poll();

                values.add(currNode.value);

                Node left = currNode.left;
                Node right = currNode.right;

                if (left != null)
                    queue.add(left);

                if (right != null)
                    queue.add(right);
            }
        return values;
    }
    /**
     * Gets all of pairs (key,value) in MyMap
     * @return MyList with all (key,value)
     */
    public MyList getEntries(){
        ArrayDeque<Node> queue = new ArrayDeque<>();
        MyList pairs  = new MyList();
        queue.add(this.root);
        if (this.size != 0)
            while (!queue.isEmpty()) {
                Node currNode = queue.poll();

                pairs.add(new Pair<>(currNode.key,currNode.value));

                Node left = currNode.left;
                Node right = currNode.right;

                if (left != null)
                    queue.add(left);

                if (right != null)
                    queue.add(right);
            }
        return pairs;
    }

    /**
     * @return Number of pairs stored in MyMap
     */
    public int size(){
        return this.size;
    }

    /**
     * @return true if MyList is empty
     */
    public  boolean isEmpty(){
        return this.size == 0;
    }
}
