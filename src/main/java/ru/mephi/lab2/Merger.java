package ru.mephi.lab2;
import ru.mephi.lab1.MyList;

public class Merger{
    /**
     * Merge two sorted MyLists in ascending order
     * @param list1 sorted MyList of ints
     * @param list2 sorted MyList of ints
     * @return Merged MyList
     */
    public static MyList merge(MyList list1, MyList list2){
        MyList result = new MyList();
        int i1 = 0;
        int i2 = 0;
        int val1;
        int val2;
        while (i1 < list1.size() && i2 <list2.size()){
            try {
                 val1 = (int)(list1.get(i1));
                 val2 = (int)(list2.get(i2));
            } catch (ClassCastException e) {
                return new MyList();
            }
            if (val1 < val2) {
                result.add(val1);
                ++i1;
            } else {
                result.add(val2);
                ++i2;
            }
        }
        if (i1 == list1.size())
            for (int i = i2; i < list2.size(); ++i) {
                    try {
                        val2 = (int)(list2.get(i));
                    } catch (ClassCastException e) {
                        return new MyList();
                    }
                    result.add(val2);
            }
            else
            for (int i = i1; i < list1.size(); ++i) {
                try {
                    val1 = (int)(list1.get(i));
                } catch (ClassCastException e) {
                    return new MyList();
                }
                result.add(val1);
            }
        return result;
    }
}
