package ru.mephi.lab2.classwork;

import java.util.*;
public class Main {
	public static void main(String[] args) {
		HashMap<String, Integer> map = new HashMap<>();
		Queue<String> q = new ArrayDeque<>();
		Scanner in = new Scanner(System.in);
		while (in.hasNextLine()){
			String[] wordsLine = in.nextLine().split("[,]?[.]?[:]?[!]?[?]?[ ]");
			for (String word : wordsLine) {
				if (!word.equals("") && map.containsKey(word))
					map.replace(word, map.get(word) + 1);
				else {
					map.put(word, 1);
					q.add(word);
				}

			}
		}
		in.close();

		map.forEach((key,value)-> System.out.println(key + "->" + value));

		StringBuilder b = new StringBuilder();
		q.forEach(word -> b.append(word).append(" "));
		System.out.println(b.toString());
	}

}
