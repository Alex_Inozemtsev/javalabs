package ru.mephi.lab3;

import java.util.List;

public class Accountant {
    public static void paySalary(Employee employee){
        System.out.println("Salary paid to " +
                employee.getGivenName() + ' ' +
                employee.getSurName());
    }
    public static void payPremium(Employee employee){
        System.out.println("Premium " +
                employee.getRole().getPremium() +
                "% paid to " +
                employee.getGivenName() + ' ' +
                employee.getSurName());
    }

    public static void payPFemale(List<Employee> employees){
        employees.stream()
                .filter(e -> e.getGender().equals(Gender.FEMALE))
                .forEach(Accountant::payPremium);
    }
    public static void paySManagers(List<Employee> employees){
        employees.stream()
                .filter(e -> e.getRole().equals(Role.MANAGER))
                .forEach(Accountant::paySalary);
    }
    public static void paySFinance(List<Employee> employees){
        employees.stream()
                .filter(e -> e.getDept().equals("Finance"))
                .forEach(Accountant::paySalary);
    }
    public static void payPStaff(List<Employee> employees){
        employees.stream()
                .filter(e -> e.getRole().equals(Role.STAFF))
                .forEach(Accountant::payPremium);
    }

}
