package ru.mephi.lab3;


import java.util.LinkedList;
import java.util.function.*;

public class Employee {
    private final String givenName;
    private final String surName;
    private int age;
    private final Gender gender;
    private Role role;

    private String dept;
    private String email;
    private String phone;
    private String address;
    private String city;
    private String state;
    private int code;
    private int salary;

    private Employee(Builder builder) {
        this.givenName = builder.givenName;
        this.surName = builder.surName;
        this.address = builder.address;
        this.salary = builder.salary;
        this.state = builder.state;
        this.dept = builder.dept;
        this.age = builder.age;
        this.role = builder.role;
        this.email = builder.email;
        this.phone = builder.phone;
        this.code = builder.code;
        this.city = builder.city;
        this.gender = builder.gender;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "givenName='" + givenName + '\'' +
                ", surName='" + surName + '\'' +
                ", age=" + age +
                ", salary='" + salary + '\'' +
                ", gender=" + gender +
                ", role=" + role +
                ", dept='" + dept + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", code=" + code +
                '}';
    }

    public String getSurName() {
        return surName;
    }

    public int getSalary() {
        return salary;
    }

    public String getGivenName() {
        return givenName;
    }

    public int getAge() {
        return age;
    }

    public Role getRole() {
        return role;
    }

    public String getDept() {
        return dept;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public Gender getGender() {
        return gender;
    }

    public String getCity() {
        return city;
    }

    public int getCode() {
        return code;
    }

    public String getState() {
        return state;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public static LinkedList<Employee> createShortList() {
        Employee e1 = new Builder()
                .givenName("Oleg")
                .surName("Ivanov")
                .age(30)
                .gender(Gender.MALE)
                .role(Role.STAFF)
                .dept("Sales")
                .email("oivanov@ex.com")
                .phone("89234360187")
                .address("")
                .salary(1000)
                .build();
        Employee e2 = new Builder()
                .givenName("Evena")
                .surName("Gavrilova")
                .age(23)
                .gender(Gender.FEMALE)
                .role(Role.STAFF)
                .dept("Finance")
                .email("egavrilova@ex.com")
                .phone("89453221401")
                .address("")
                .salary(2000)
                .build();
        Employee e3 = new Builder()
                .givenName("Oleg")
                .surName("Panin")
                .age(25)
                .gender(Gender.MALE)
                .role(Role.STAFF)
                .dept("Sales")
                .email("opanin@ex.com")
                .phone("89384962765")
                .address("")
                .salary(1500)
                .build();
        Employee e4 = new Builder()
                .givenName("Petr")
                .surName("Korovin")
                .age(22)
                .gender(Gender.MALE)
                .role(Role.STAFF)
                .dept("IT")
                .email("pkorovin@ex.com")
                .phone("89342378410")
                .address("")
                .salary(1300)
                .build();
        Employee e5 = new Builder()
                .givenName("Igor")
                .surName("Sidorov")
                .age(27)
                .gender(Gender.MALE)
                .role(Role.STAFF)
                .dept("IT")
                .email("isidorov@ex.com")
                .phone("89213256782")
                .address("")
                .salary(900)
                .build();
        Employee e6 = new Builder()
                .givenName("Vera")
                .surName("Ivanova")
                .age(33)
                .gender(Gender.FEMALE)
                .role(Role.MANAGER)
                .dept("Sales")
                .email("vivanova@ex.com")
                .phone("89450986545")
                .address("")
                .salary(3000)
                .build();
        Employee e7 = new Builder()
                .givenName("Vladimir")
                .surName("Petrov")
                .age(35)
                .gender(Gender.MALE)
                .role(Role.MANAGER)
                .dept("Sales")
                .email("vpetrov@ex.com")
                .phone("89437614927")
                .address("")
                .salary(1700)
                .build();
        Employee e8 = new Builder()
                .givenName("Alex")
                .surName("Inozemtsev")
                .age(20)
                .gender(Gender.MALE)
                .role(Role.MANAGER)
                .dept("IT")
                .email("ainozemtsev@ex.com")
                .phone("894572143")
                .address("")
                .salary(1100)
                .build();
        Employee e9 = new Builder()
                .givenName("Olga")
                .surName("Mironova")
                .age(28)
                .gender(Gender.FEMALE)
                .role(Role.EXECUTIVE)
                .dept("Finance")
                .email("omironova@ex.com")
                .phone("89234360187")
                .address("")
                .salary(3500)
                .build();
        Employee e10 = new Builder()
                .givenName("Nikita")
                .surName("Nikitin")
                .age(24)
                .gender(Gender.MALE)
                .role(Role.EXECUTIVE)
                .dept("IT")
                .email("nnikitin@ex.com")
                .phone("89871236543")
                .address("")
                .salary(2300)
                .build();
        LinkedList<Employee> employees = new LinkedList<>();
        employees.add(e1);
        employees.add(e2);
        employees.add(e3);
        employees.add(e4);
        employees.add(e5);
        employees.add(e6);
        employees.add(e7);
        employees.add(e8);
        employees.add(e9);
        employees.add(e10);
        return employees;
    }

    private static final Consumer<String> simpleSender =
            s -> System.out.println("Email was sent on " + s);

    public void sendMail(Consumer<String> sender) {
        sender.accept(this.getEmail());
    }

    private static final IntFunction<String> adultInfo =
            age -> {
                if (age >= 18)
                    return "Employee is adult";
                else
                    return "Employee is not adult";
            };

    public void info(IntFunction<String> informer) {
        informer.apply(this.getAge());
    }

    public Supplier<String> emailGen =
            () -> this.getGivenName().toLowerCase().charAt(0)
                    + this.getSurName().toLowerCase()
                    + "@ex.com";

    private static final Predicate<String> correctEmail =
            s -> s.matches("^[A-Za-z0-9_.]+[@](.+)[.](.+)$");

    private static boolean isCorrect(String s, Predicate<String> checker) {
        return checker.test(s);
    }

    public boolean isEmailCorrect() {
        return isCorrect(this.getEmail(), correctEmail);
    }

    public static class Builder {

        private String givenName;
        private String surName;
        private int age;
        private Gender gender;
        private Role role;
        private int salary;
        private String dept;
        private String email;
        private String phone;
        private String address;
        private String city = "Moscow";
        private String state = "MSK";
        private int code = 1;


        public Builder givenName(String name) {
            givenName = name;
            return this;
        }

        public Builder age(int a) {
            age = a;
            return this;
        }

        public Builder surName(String sur) {
            surName = sur;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder salary(int s) {
            salary = s;
            return this;
        }

        public Builder dept(String val) {
            dept = val;
            return this;
        }

        public Builder role(Role val) {
            role = val;
            return this;
        }

        public Builder gender(Gender g) {
            gender = g;
            return this;
        }

        public Builder phone(String p) {
            phone = p;
            return this;
        }

        public Builder state(String s) {
            state = s;
            return this;
        }

        public Builder code(int c) {
            code = c;
            return this;
        }

        public Builder address(String a) {
            address = a;
            return this;
        }

        public Builder city(String c) {
            city = c;
            return this;
        }

        public Employee build() {
            return new Employee(this);
        }
    }
}
