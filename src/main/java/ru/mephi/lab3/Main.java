package ru.mephi.lab3;

import java.util.LinkedList;

public class Main {
    public static void main(String[] args){
        LinkedList<Employee> employees = Employee.createShortList();

        employees.forEach(System.out::println);

        Accountant.payPFemale(employees);
        employees.get(0).setEmail(employees.get(0).emailGen.get());
        System.out.println(employees.get(0).isEmailCorrect());
    }
}
