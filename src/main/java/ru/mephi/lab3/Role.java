package ru.mephi.lab3;

enum Role {
    STAFF(10),
    MANAGER(20),
    EXECUTIVE(30);

    private int premium;

    public int getPremium(){
        return this.premium;
    }
    public void setPremium(int p){
        if (  premium >= 0)
            this.premium = p;
    }

    Role(int premium){
        this.premium = premium;
    }
}
