package ru.mephi.lab4;

import ru.mephi.lab3.Employee;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Map;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args){
        StreamUser u = new StreamUser();
        LinkedList<Employee> employees = Employee.createShortList();
        employees.forEach(System.out::println);
        System.out.println("==============================");
        System.out.println("Most frequent operator is " + u.mainOperator(employees));

        System.out.println("==============================");
        System.out.println("First after 2000: " + u.firstAfter2000(employees));

        System.out.println("==============================");
        System.out.println("Difference between max and average: " + u.diff(employees));

        System.out.println("==============================");
        System.out.println("Before salary increase: " + u.salarySum(employees));
        System.out.println("If salary increases up to 2000: " + u.newSalarySum(employees));



    }

}
