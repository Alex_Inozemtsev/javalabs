package ru.mephi.lab4;

import ru.mephi.lab3.Employee;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

public class StreamUser {

    // Converts a phone code to operator name.
    private final IntFunction<String> toOperator =
            code -> {
                if (code >= 900 & code < 920)
                    return "Operator1";
                if (code >= 920 & code < 940)
                    return "Operator2";
                if (code >= 940 & code < 960)
                    return "Operator3";
                if (code >= 960 & code < 980)
                    return "Operator4";
                if (code >= 980 & code < 1000)
                    return "Operator5";

                return "Unrecognized operator";
            };

    // Name of the most frequent operator used by employees
    String mainOperator(List<Employee> employees) {
        return employees.stream()
                .map(Employee::getPhone)
                .map(num -> Integer.parseInt(num.substring(1, 4)))
                .map(toOperator::apply)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .peek(pair -> System.out.println(pair.getValue() + " emp. use " + pair.getKey()))
                .max(Map.Entry.comparingByValue())
                .get()
                .getKey();
    }

    // First employee who's salary is bigger then 2000
    Employee firstAfter2000(List<Employee> employees) {
        return employees.stream()
                .sorted(Comparator.comparing(Employee::getSalary))
                .peek(e -> System.out.println(e.getGivenName() + " " + e.getSurName() + " earns less then 2000: " + e.getSalary()))
                .filter(e -> e.getSalary() >= 2000)
                .findFirst().get();
    }

    // Difference between highest and average salary
    double diff(List<Employee> employees) {
        return employees.stream()
                .mapToInt(Employee::getSalary)
                .max()
                .getAsInt()

                -

                employees.stream()
                        .mapToInt(Employee::getSalary)
                        .average()
                        .getAsDouble();
    }

    //Total salary
    int salarySum(List<Employee> employees) {
        return employees.stream()
                .mapToInt(Employee::getSalary)
                .sum();
    }

    // Total salary if no one will earn less then 2000
    int newSalarySum(List<Employee> employees) {
        return employees.stream()
                .mapToInt(Employee::getSalary)
                .filter(salary -> salary >= 2000)
                .sum()

                +

                employees.stream()
                        .mapToInt(Employee::getSalary)
                        .filter(salary -> salary < 2000)
                        .map(salary -> 2000)
                        .sum();
    }

}
