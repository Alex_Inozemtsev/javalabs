package ru.mephi.lab1;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class MyListTest{
    private final MyList list = new MyList(new Integer[]{0,1,2,3,4});
    @Test
    public void testRemove(){
        list.remove(2);
        MyList expList = new MyList(new Integer[]{0,1,3,4});
        assertEquals(list,expList);
    }
    @Test
    public void testAddFirst(){
        list.add(5);
        MyList expList = new MyList(new Integer[]{0,1,2,3,4,5});
        assertEquals(list,expList);
    }
    @Test
    public void testAdd(){
        list.add(300,4);
        MyList expList = new MyList(new Integer[]{0,1,2,3,300,4});
        assertEquals(list,expList);
    }
    @Test
    public void testSet(){
        list.set(300,4);
        MyList expList = new MyList(new Integer[]{0,1,2,3,300});
        assertEquals(list,expList);
    }
    @Test
    public void testGet(){
        assertEquals( list.get(3),3);
    }
    @Test
    public void testContains(){
        assertTrue(list.contains(2));
    }
    @Test
    public void testIndexOf(){
        assertEquals(list.indexOf(3),3);
    }
    @Test
    public void testSize(){
        assertEquals(list.size(),5);
    }
    @Test
    public void testIsEmpty(){
        assertFalse(list.isEmpty());
    }


}

