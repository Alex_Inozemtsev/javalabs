package ru.mephi.lab2;

import org.junit.jupiter.api.Test;
import ru.mephi.lab1.MyList;

import static org.junit.jupiter.api.Assertions.*;

class MergerTest {

    @Test
    void merge() {
        MyList list1 = new MyList(new Object[]{1,4,7,8,9,10});
        MyList list2 = new MyList(new Object[]{2,6});
        MyList expList = new MyList(new Object[]{1,2,4,6,7,8,9,10});
        assertEquals(expList,Merger.merge(list1,list2));
    }
}